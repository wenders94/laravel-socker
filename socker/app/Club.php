<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public const PROBABILITY = 5; // вероятность в процентном отношении забить гол в минуту
}
