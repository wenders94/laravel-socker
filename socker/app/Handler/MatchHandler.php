<?php

declare(strict_types=1);

namespace App\Handler;

use App\Club;
use App\Match;
use App\Player;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class MatchHandler
{
    private $clubs;

    public function __construct(Collection $clubs)
    {
        $this->validateCountClub($clubs);

        $this->clubs = $clubs;
    }

    /**
     * @param Collection $clubs
     * @throws Exception
     */
    private function validateCountClub(Collection $clubs): void
    {
        if (count($clubs) != Match::COUNT_CLUBS) {
            throw new Exception('Команд должно быть ' . Match::COUNT_CLUBS);
        }
    }

    /**
     * @param int $allTime
     * @return array
     */
    public function getResultMatch(int $allTime): array
    {
        $result = [];

        for ($minute = 1; $minute <= $allTime; $minute++) {
            foreach ($this->clubs as $club) {
                $result[$club->id] = $result[$club->id] ?? [];

                if (mt_rand(1, 100) <= Club::PROBABILITY) {
                    $result[$club->id][Player::findRandomForward($club->id)][] = $minute;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $matchResults
     * @return array
     */
    public static function getCountGoals(array $matchResults): array
    {
        $totalGoals = [];

        foreach ($matchResults as $matchResult) {
            $countGoals = 0;

            if (!empty($matchResult)) {
                foreach ($matchResult as $player => $minutes) {
                    $countGoals += count($minutes);
                }
            }

            $totalGoals[] = $countGoals;
        }

        return $totalGoals;
    }

    /**
     * @param array $matchResults
     * @return string
     */
    public static function responseShortCode(array $matchResults): string
    {
        $response = '';
        $totalGoals = self::getCountGoals($matchResults);

        foreach ($matchResults as $club => $matchResult) {
            $response .= Club::find($club)->name;

            if (array_key_last($matchResults) == $club) {
                $response .= '<br />';
            } else {
                $response .= ' - ';
            }
        }

        $response .= 'Счёт: ';

        foreach ($totalGoals as $club => $goals) {
            $response .= $goals;

            if (array_key_last($matchResults) == $club) {
                $response .= '<br />';
            } else {
                $response .= ':';
            }
        }

        $response .= 'Голы: ';

        foreach ($matchResults as $club => $matchResult) {
            $clubName = Club::find($club)->name;

            if (is_array($matchResult)) {
                foreach ($matchResult as $player => $minutes) {
                    $namePlayer = Player::find($player)->name;

                    $response .= "{$namePlayer} ({$clubName}, минуты: " . implode(',', $minutes) . ') ';
                }
            }

            if (array_key_last($matchResults) != $club && !empty($matchResult)) {
                $response .= ' - ';
            }
        }

        return $response;
    }
}
