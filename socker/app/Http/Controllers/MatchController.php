<?php

namespace App\Http\Controllers;

use App\Club;
use App\Handler\MatchHandler;
use App\Match;
use App\Player;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MatchController extends Controller
{
    /**
     * @return string
     * @throws Exception
     */
    public function startMatch(): string
    {
        $randomClubs = Club::all()->random(Match::COUNT_CLUBS);
        $allTime = Match::COUNT_HALF * Match::TIME_HALF;
        $match = new MatchHandler($randomClubs);
        $result = $match->getResultMatch((int)$allTime);
        $totalGoals = MatchHandler::getCountGoals($result);

        $match = Match::create(
            [
                'count_clubs' => Match::COUNT_CLUBS,
                'time_half' => Match::TIME_HALF,
                'count_half' => Match::COUNT_HALF,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'team1_goals' => $totalGoals[0],
                'team2_goals' => $totalGoals[1],
                'match_result' => json_encode($result)
            ]
        );

        $match->save();

        return redirect("/match/{$match->id}");
    }

    public function getMatch(Match $match): string
    {
        $matchResults = json_decode($match->match_result, true);
        $response = MatchHandler::responseShortCode($matchResults);

        return view(
            'match.index',
            [
                'result' => $response,
            ]
        );
    }
}
