<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public const COUNT_CLUBS = 2;
    public const COUNT_HALF = 2;
    public const TIME_HALF = 45;

    protected $fillable = [
        'count_clubs',
        'time_half',
        'count_half',
        'created_at',
        'updated_at',
        'team1_goals',
        'team2_goals',
        'match_result'
    ];
}
