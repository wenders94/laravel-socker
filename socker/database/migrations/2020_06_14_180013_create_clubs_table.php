<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'clubs',
            function (Blueprint $table) {
                $table->id();
                $table->string('name', 20);
                $table->integer('count_players')->default(11);
                $table->timestamps();
            }
        );

        Schema::table(
            'players',
            function (Blueprint $table) {
                $table->foreign('club_id')->references('id')->on('clubs');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
