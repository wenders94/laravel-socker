<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'matches',
            function (Blueprint $table) {
                $table->id();
                $table->integer('count_clubs')->default('2');
                $table->integer('time_half')->default('45');
                $table->integer('count_half')->default('2');
                $table->integer('team1_goals');
                $table->integer('team2_goals');
                $table->text('match_result');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
