<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->insert(
            [
                // Динамо
                [
                    'name' => 'БУЩАН',
                    'number' => 1,
                    'role' => 'goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ПОПОВ',
                    'number' => 4,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'МИКОЛЕНКО',
                    'number' => 16,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ПИВАРИЧ',
                    'number' => 23,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'БУРДА',
                    'number' => 26,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ШАБАНОВ',
                    'number' => 30,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'КЕНДЗЁРА',
                    'number' => 94,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'СИДОРЧУК',
                    'number' => 5,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'КАДИРИ',
                    'number' => 6,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ВЕРБИЧ',
                    'number' => 7,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ШЕПЕЛЕВ',
                    'number' => 8,
                    'role' => 'not_goalkeeper',
                    'club_id' => 1,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],

                // Шахтёр
                [
                    'name' => 'Пятов',
                    'number' => 30,
                    'role' => 'goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'ШАПАРЕНКО',
                    'number' => 10,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Степаненко',
                    'number' => 6,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Тайсон',
                    'number' => 7,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Фернандо',
                    'number' => 99,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Антонио',
                    'number' => 8,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Дентиньо',
                    'number' => 9,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Марлос',
                    'number' => 11,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Болбат',
                    'number' => 50,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Тете',
                    'number' => 14,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Коноплянка',
                    'number' => 15,
                    'role' => 'not_goalkeeper',
                    'club_id' => 2,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]
            ]
        );
    }
}
